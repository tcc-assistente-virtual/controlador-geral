(ns server_controller.db)

(def db
  {:classname "org.postgresql.Driver"
   :subprotocol "postgresql"
   :subname "//127.0.0.1:5432/db"
   :user "user"
   :password "pass"
   })